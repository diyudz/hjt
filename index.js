/*
comentarios de un video

*/

var express = require("express");
var app = express();

var bodyParser = require("body-parser");
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
app.use(bodyParser.json());

app.use('/', express.static('public'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

require("./base.js")(app);

app.get("/tumblr/post/:tag",function(req, res){
	var tag = req.params["tag"];
	console.log(req.params);
	// Authenticate via API Key
	var tumblr = require('tumblr.js');
	var client = tumblr.createClient({
		consumer_key: '2YVZRpcwATQNgthuvib1T8t8sQUzmSE5oIHV2RsSsWSDksJ9U8'
	});
	// Make the request
	client.tagged(tag, function (err, data) {
		if(err){
			return res.send(err);
		}
	    res.send(data);
	});
});

app.get("/foursquare/:lugar",function(req, res){
    var lugar = req.params['lugar'];
    
    var params = {
    	"ll": "17.0777928,-96.73830040000001", 
        "query": lugar,
        "limit": 5,
        "radius": 10000
    };
    var foursquare = (require('foursquarevenues'))('4KC51MNV23YZKLT34Y0TS4PO0J1VKQWESEMBUVD43PDACMLF', 'VW20AHQLJLEYRYHCH3ZCX2244F4OGK5ZSXINZDMB2LUA0WRF');
    
    foursquare.getVenues(params, function(error, venues) {
        if (!error) {
        	var respuesta = []
        	var lugares = venues.response.venues;
        	for (var i = 0; i < lugares.length; i++) {
        		respuesta.push({
        			"nombre": lugares[i].name,
        			"direccion": lugares[i].location.address,
        			"lat": lugares[i].location.lat,
        			"lon": lugares[i].location.lng
        		});
        	};
            return res.send(respuesta);
        }else{
            return res.send(error);
        }
    });
});

app.get("/tumblr/imagenes/:tag",function(req, res){
	var tag = req.params["tag"];
	//console.log(req.params);
   // Authenticate via API Key
	var tumblr = require('tumblr.js');
	var client = tumblr.createClient({
		consumer_key: '2YVZRpcwATQNgthuvib1T8t8sQUzmSE5oIHV2RsSsWSDksJ9U8'
	});
	// Make the request
	client.tagged(tag, function (err, data) {
		if(err){
			return res.send(err);
		}
      var imagenes = [];
      var contador = 0;
      for (var i = 0; i < data.length; i++) {
         if(data[i].type === "photo"){
               imagenes[contador]= data[i].photos[0].alt_sizes[2].url;
               contador++;
         }
         if(contador>4) break;
      }
      res.send(imagenes);
	});
});

app.post("/plurk/add",function(req, res){
   var contenido = req.body.contenido;
	var PlurkClient = require('plurk').PlurkClient;
	var client = new PlurkClient(true, "oOAZAWKgw5Bt", "VJZvwMcNX4ditlnKRUv2xkyShwv0Newh", "hCixvTCRwDi2", "jc2A9soYz04fFMCXRxIpgU54wulbcESK");
	// https: true or false, for requesting request token and access token
	// accessToken and accessTokenSecret are optional and can be set afterward.
	client.accessToken = "hCixvTCRwDi2";
	client.accessTokenSecret = "jc2A9soYz04fFMCXRxIpgU54wulbcESK";

	client.rq('Timeline/plurkAdd', {'content': contenido, 'qualifier':'is','lang':'es'}, function(err, data) {
    	if (!err){
    		return res.send(data);
    	}
    	return res.send(err);
	});
});


var CREDENTIALS = {"web":
	{
		"client_id": "211830225489-2s1ho7hv0kt74migh6rfe4c2lg0nrbcm.apps.googleusercontent.com",
		"auth_uri":"https://accounts.google.com/o/oauth2/auth",
		"token_uri":"https://accounts.google.com/o/oauth2/token",
		"auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
		"client_secret":"Ufw4mqvu-Kc4vcB8iA8jzZjn"
    }
};

app.get('/youtube/:tag', function(req, res){
	var tag = req.params["tag"];

	var YouTube = require('youtube-node');

	var youTube = new YouTube();

	youTube.setKey('AIzaSyDV4PMbXuIl7qYzdfboj3svsjmTZ8cMqRs');

	youTube.search(tag, 30, function(error, result) {
	  if (error) {
	    return res.send(error);
	  }
	  else {
	    return res.send(result);
	  }
	});

});

app.get('/youtube/etiquetas/:id', function(req, res){
	var id = req.params["id"];
   var YouTube = require('youtube-node');
	var youTube = new YouTube();
	youTube.setKey('AIzaSyDV4PMbXuIl7qYzdfboj3svsjmTZ8cMqRs');
   //HcwTxRuq-uk
   youTube.getById(id, function(error, result) {
     if (error) {
       console.log(error);
     }
     else {
       var snippet = result.items[0].snippet;
       var tags = ["nada","tumblr"];

       if(snippet.tags != undefined){
          tags = snippet.tags;  
          for (var i = 0; i < tags.length; i++) {
            tags[i] =  tags[i].replace(/\ /g, "");
         } 
       }
       var respuesta = {
          "url": id,
          "nombre": snippet.title,
          "descripcion": snippet.description,
          "etiquetas": tags
       }
       return res.send(respuesta);
     }
   });

});

app.listen(process.env.OPENSHIFT_NODEJS_PORT || 8080, process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',function(){
	console.log("Corriendo");
});
