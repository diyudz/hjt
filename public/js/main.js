var main = angular.module("main", ['ngRoute']);

main.config(function($routeProvider){
	$routeProvider.when('/home', {
		controller: 'mainController',
		templateUrl: 'templates/main.html'
	}).when('/video/:id/:tag', {
		controller: 'videoController',
		templateUrl: 'templates/video.html'
	}).otherwise({
		redirectTo: '/home'
	});
});

main.controller("mainController", function($scope, $rootScope, $http){

	$scope.videos = [];

	$scope.buscar = function(){
		if($scope.tag=='') return false;
		$rootScope.tag = $scope.tag;
		$http.get("/youtube/"+$scope.tag).success(function(data){
			var aux = [];
			for(var i in data.items){
				if(data.items[i].id.kind == "youtube#video"){
					aux.push({
						id: data.items[i].id.videoId,
						titulo: data.items[i].snippet.title,
						imagen: data.items[i].snippet.thumbnails.medium.url,
						fecha: data.items[i].snippet.publishedAt
					});
				}
			}
			$scope.videos = aux;
		}).error(function(err){
			console.log("Ocurrio un error");
			console.log(err);
		})

	}

});

main.controller('videoController', function($scope, $routeParams,$rootScope,  $http){
	//$routeParams.id);
	initMap();
	$scope.video = {
		publicado: false
	};
	$scope.video.fullurl = "http://www.youtube.com/embed/"+$routeParams.id+"?autoplay=0";

	var iframe = document.createElement("iframe");
	iframe.src = $scope.video.fullurl.replace(/^http/i, "https");
	iframe.width = 480;
	iframe.height = 320;
	document.getElementById("video").appendChild(iframe);

	$scope.imagenes = [];

	$http.get("/youtube/etiquetas/"+$routeParams.id).success(function(data){
		// url, nombre, descripcion, etiquetas
		data.etiquetas = (function(){
			var et = [];
			for(var i = 0; i < 5; i++) et.push(data.etiquetas[i]);
			return et;
		})();

		console.log(data.etiquetas[0]);
		$http.get("/tumblr/imagenes/"+data.etiquetas[1]).success(function(data){
			$scope.imagenes = data;
		}).error(function(err){
			console.log("Ocurrio un error");
			console.log(err);
		});
		data.publicado = $scope.video.publicado;
		data.fullurl = $scope.video.fullurl;
		$scope.video = data;

		$http.get("/foursquare/"+$routeParams.tag).success(function(lugares){
			for(var i in markers) markers[i].setMap(null);
				$scope.lugares = lugares;
			for(var i in lugares){
				var lugar = lugares[i];
				var marca = new google.maps.Marker({
					position: new google.maps.LatLng(lugar.lat, lugar.lon),
					map: map,
					icon: 'marcas/icon2.png',
					title: lugar.nombre,
					label: lugar.descripcion
				});
				markers.push(marca);
			}
		}).error(function(err){
			console.log("Ocurrio un error");
			console.log(err);
		});

	}).error(function(err){
		console.log("Ocurrio un error");
		console.log(err);
	});

	$scope.publicar = function(){
		var contenido = " viendo "+$scope.video.fullurl+" ";
		
		for(var i in $scope.video.etiquetas){
			contenido += "#"+$scope.video.etiquetas[i]+" ";
		}

		$http.post("/plurk/add", {contenido: contenido}).success(function(data){
			$scope.video.publicado = true;
			//swal("Listo!", "Publicado exitosamente!", "success");
		});
	}
});